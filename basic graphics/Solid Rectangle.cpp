//-----------------------------------------------------------------------------
// Basic Graphics Drawing Using Direct 3D
// Draw a solid coloured rectangle

#define D3D_DEBUG_INFO

//-----------------------------------------------------------------------------
// Include these files
#include <Windows.h> // Windows library (for window functions, menus, dialog boxes, etc)
#include <Windowsx.h>
#include <d3dx9.h> // Direct 3D library (for all Direct 3D funtions).
#include <cmath>
#include "HexColors.h"
#include "rgb-hsv_conversion.h"

//-----------------------------------------------------------------------------
// Global variables

LPDIRECT3D9 g_pD3D = NULL;             // Used to create the D3DDevice
LPDIRECT3DDEVICE9 g_pd3dDevice = NULL; // The rendering device
LPDIRECT3DVERTEXBUFFER9 g_pVBSquare =
    NULL; // Buffer to hold vertices for the rectangle
LPDIRECT3DVERTEXBUFFER9 g_pVBCross = NULL; // Buffer for cross
LPDIRECT3DVERTEXBUFFER9 g_pVB3DStep = NULL; // Buffer for 3D step
LPDIRECT3DVERTEXBUFFER9 g_pVBCone = NULL; // Buffer for cone
LPDIRECT3DVERTEXBUFFER9 g_pVBSpherePoints = NULL; // Buffer for sphere
LPDIRECT3DVERTEXBUFFER9 g_pVBSphereTris = NULL; // Buffer for sphere

// A structure for our custom vertex type
struct CUSTOMVERTEX {
    float x, y, z; // X, Y, Z position of the vertex.
    DWORD colour;  // The vertex color
};

// The structure of a vertex in our vertex buffer...
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE)

// Mouse look
bool gbMouseSpin = false; // When true, moving the mouse rotates the model
short gMouseX;
short gMouseY;
short gPrevMouseX;
short gPrevMouseY;
float gCameraAzimuth = 0;
float gCameraPolar = D3DX_PI / 2;
float gCameraDistance = 30.0f;

//-----------------------------------------------------------------------------
// Initialise Direct 3D.
// Requires a handle to the window in which the graphics will be drawn.
HRESULT SetupD3D(HWND hWnd) {
    // Create the D3D object, return failure if this can't be done.
    if (NULL == (g_pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
        return E_FAIL;

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory(&d3dpp, sizeof(d3dpp));
    d3dpp.Windowed = TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
    d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

    // Create the D3DDevice
    if (FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                    D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp,
                                    &g_pd3dDevice))) {
        return E_FAIL;
    }

    // Turn on the Z buffer
    g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

    // Turn off the lighting, as we're using our own vertex colours.
    g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

    return S_OK;
}

//-----------------------------------------------------------------------------
// Release (delete) all the resources used by this program.
// Only release things if they are valid (i.e. have a valid pointer).
// If not, the program will crash at this point.
void CleanUp() {
    if (g_pVBSquare != NULL)
        g_pVBSquare->Release();
    if (g_pVBCross != NULL)
        g_pVBCross->Release();
    if (g_pVB3DStep != NULL)
        g_pVB3DStep-> Release();
    if (g_pVBCone != NULL)
        g_pVBCone-> Release();
    if (g_pVBSpherePoints != NULL)
        g_pVBSpherePoints->Release();    
    if (g_pVBSphereTris != NULL)
        g_pVBSphereTris->Release();
    if (g_pd3dDevice != NULL)
        g_pd3dDevice->Release();
    if (g_pD3D != NULL)
        g_pD3D->Release();
}

//-----------------------------------------------------------------------------
// Set up the scene geometry.
HRESULT Setup3DStepVertices() {
    // Rendering as a single triangle strip
    int Vertices = 22;
    int BufferSize = Vertices * sizeof(CUSTOMVERTEX);
    if (FAILED(g_pd3dDevice->CreateVertexBuffer(
            BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT,
            &g_pVB3DStep, NULL))) {
        return E_FAIL; 
    }
    CUSTOMVERTEX *pVertices;
    if (FAILED(g_pVB3DStep->Lock(0, 0, (void **)&pVertices, 0))) {
        return E_FAIL;
    }

    // Triangle strip verts
    pVertices[0] = { -10, -5, -5, Colors::RED };
    pVertices[1] = { -10,  0, -5, Colors::RED };
    pVertices[2] = {  10, -5, -5, Colors::RED };
    pVertices[3] = {   5,  0, -5, Colors::YELLOW };
    pVertices[4] = { 10, 5, -5, Colors::GREEN };
    pVertices[5] = { 5,  5, -5, Colors::CYAN };
    pVertices[6] = { 5,  5, 5, Colors::BLUE };
    pVertices[7] = { 5, 0, -5, Colors::MAGENTA };
    pVertices[8] = { 5, 0, 5, Colors::RED };
    pVertices[9] = { -10, 0, -5, Colors::TWILIGHT };
    pVertices[10] = { -10, 0, 5, Colors::FLUTTERSHY };
    pVertices[11] = { -10, -5, -5, Colors::APPLEJACK };
    pVertices[12] = { -10, -5, 5, Colors:: RED};
    pVertices[13] = { 10, -5, -5, Colors::YELLOW };
    pVertices[14] = { 10, -5, 5, Colors::GREEN };
    pVertices[15] = { 10, 5, -5, Colors::CYAN };
    pVertices[16] = { 10, 5, 5, Colors::BLUE };
    pVertices[17] = { 5, 5, 5, Colors::MAGENTA };
    pVertices[18] = { 10, -5, 5, Colors::RED };
    pVertices[19] = { 5, 0, 5, Colors::RED };
    pVertices[20] = { -10, -5, 5, Colors::TWILIGHT };
    pVertices[21] = { -10, 0, 5, Colors::FLUTTERSHY };

    g_pVB3DStep->Unlock();
    return S_OK;
}

HRESULT SetupConeVertices() {
    // Rendering as a single triangle fan
    const int NUM_FACES = 20;
    const float HEIGHT = 20.0f;
    const float RADIUS = 5.0f;
    int Vertices = NUM_FACES + 2;
    int BufferSize = Vertices * sizeof(CUSTOMVERTEX);
    if (FAILED(g_pd3dDevice->CreateVertexBuffer(
        BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT,
        &g_pVBCone, NULL))) {
        return E_FAIL;
    }
    CUSTOMVERTEX *pVertices;
    if (FAILED(g_pVBCone->Lock(0, 0, (void **)&pVertices, 0))) {
        return E_FAIL;
    }
    // tip vertex
    pVertices[0] = { 0, HEIGHT, 0, Colors::WHITE };
    // base circle vertices
    float angle = 0;
    const float deltaAngle = 2 * D3DX_PI / NUM_FACES;
    for (int i = 0; i != Vertices - 1; ++i) {
        pVertices[i + 1] = { RADIUS * sinf(angle), 0, RADIUS * cosf(angle), Colors::BLUE };
        angle += deltaAngle;
    }
    g_pVBCone->Unlock();
    return S_OK;
}

HRESULT SetupSphereVerticesPoints() {
    const float RADIUS = 10;
    const int SUBDIVISIONS_HEIGHT = 21;
    const int SUBDIVISIONS_AXIS = 21;

    int Vertices = (SUBDIVISIONS_HEIGHT + 1) * (SUBDIVISIONS_AXIS + 1);
    int BufferSize = Vertices * sizeof(CUSTOMVERTEX);
    if (FAILED(g_pd3dDevice->CreateVertexBuffer(
        BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT,
        &g_pVBSpherePoints, NULL))) {
        return E_FAIL;
    }
    CUSTOMVERTEX *pVertices;
    if (FAILED(g_pVBSpherePoints->Lock(0, 0, (void **)&pVertices, 0))) {
        return E_FAIL;
    }

    const float deltaAzi = 2 * D3DX_PI / SUBDIVISIONS_AXIS;
    const float deltaPolar = 2 * D3DX_PI / SUBDIVISIONS_HEIGHT;
    float azimuth = 0.0f;
    float polar = D3DX_PI / 2;
    // For each polar angle
    for (int i = 0; i <= SUBDIVISIONS_HEIGHT; ++i) {
        // Add points around the azimuth
        for (int j = 0; j <= SUBDIVISIONS_AXIS; ++j) {
            pVertices[i*(SUBDIVISIONS_AXIS + 1) + j] = {
                RADIUS * cosf(polar) * cosf(azimuth), 
                RADIUS * sinf(polar), 
                RADIUS * cosf(polar) * -sinf(azimuth), 
                Colors::RED };
            azimuth += deltaAzi;
        }
        polar -= deltaPolar;
        azimuth = 0.0f;
    }
    g_pVBSpherePoints->Unlock();
    return S_OK;
}

HRESULT SetupSphereVerticesTris() {
    const float RADIUS = 10;
    const int SUBDIVISIONS_HEIGHT = 21;
    const int SUBDIVISIONS_AXIS = 21;

    int Vertices = SUBDIVISIONS_HEIGHT * (SUBDIVISIONS_AXIS + 1) * 2 +
                   (SUBDIVISIONS_HEIGHT-1)*2; // guess
    int BufferSize = Vertices * sizeof(CUSTOMVERTEX);
    if (FAILED(g_pd3dDevice->CreateVertexBuffer(
            BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT,
            &g_pVBSphereTris, NULL))) {
        return E_FAIL;
    }
    CUSTOMVERTEX *pVertices;
    if (FAILED(g_pVBSphereTris->Lock(0, 0, (void **)&pVertices, 0))) {
        return E_FAIL;
    }

    const float deltaAzi = 2 * D3DX_PI / SUBDIVISIONS_AXIS;
    const float deltaPolar = D3DX_PI / SUBDIVISIONS_HEIGHT;
    float azimuth = 0.0f;
    float polar = D3DX_PI / 2;
    float nextPolar = polar - deltaPolar;
    unsigned vertexIndex = 0;
    // For each polar angle
    for (int i = 0; i != SUBDIVISIONS_HEIGHT; ++i) {
		float a1 = i / SUBDIVISIONS_HEIGHT * 255;
		float a2 = (i + 1) / SUBDIVISIONS_HEIGHT * 255;
		float hue1 = (360 * i / SUBDIVISIONS_HEIGHT) % 360;
		float hue2 = (360 * (i+1) / SUBDIVISIONS_HEIGHT) % 360;
		float sat = 1.0f;
		float value = 1.0f;
		float r1, g1, b1, r2, g2, b2;
		HSVtoRGB(&r1, &g1, &b1, hue1, sat, value);
		HSVtoRGB(&r2, &g2, &b2, hue2, sat, value);
		DWORD color1 = D3DCOLOR_ARGB((int)a1, (int)(r1*255), (int)(g1*255), (int)(b1*255));
		DWORD color2 = D3DCOLOR_ARGB((int)a2, (int)(r2*255), (int)(g2*255), (int)(b2*255));
        // For each azimuth around the horizontal band
        for (int j = 0; j <= SUBDIVISIONS_AXIS; ++j) {
            pVertices[vertexIndex] = {RADIUS * cosf(polar) * cosf(azimuth), 
                                      RADIUS * sinf(polar),
                                      RADIUS * cosf(polar) * -sinf(azimuth), 
                                      color1}; // alpha doesn't seem to work??
            ++vertexIndex;
            pVertices[vertexIndex] = {RADIUS * cosf(nextPolar) * cosf(azimuth),
                                      RADIUS * sinf(nextPolar),
                                      RADIUS * cosf(nextPolar) * -sinf(azimuth),
									  color2};
            ++vertexIndex;
            azimuth += deltaAzi;
        }
        polar = nextPolar;
        nextPolar -= deltaPolar;
        azimuth = 0.0f;
        // Write degenerate triangle to set up the start of the next band
		// except on final loop
        if (i != SUBDIVISIONS_HEIGHT - 1) {
            pVertices[vertexIndex] = {RADIUS * cosf(nextPolar) * cosf(azimuth),
                                      RADIUS * sinf(nextPolar),
                                      RADIUS * cosf(nextPolar) * -sinf(azimuth),
                                      Colors::BLUE};
			++vertexIndex;
            pVertices[vertexIndex] = {RADIUS * cosf(nextPolar) * cosf(azimuth),
                                      RADIUS * sinf(nextPolar),
                                      RADIUS * cosf(nextPolar) * -sinf(azimuth),
                                      Colors::BLUE};
			++vertexIndex;
        }
    }
    g_pVBSphereTris->Unlock();
    return S_OK;
}

HRESULT SetupSquareVertices() {
    // Calculate the number of vertices required, and the size of the buffer to
    // hold them.
    int sqVertices = 2 * 3; // Six vertices for the rectangle.
    int BufferSize = sqVertices * sizeof(CUSTOMVERTEX);

    // Now get Direct3D to create the vertex buffer.
    // The vertex buffer for the rectangle doesn't exist at this point, but the
    // pointer to
    // it does (g_pVBSquare)
    if (FAILED(g_pd3dDevice->CreateVertexBuffer(
            BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT,
            &g_pVBSquare, NULL))) {
        return E_FAIL; // if the vertex buffer culd not be created.
    }

    // Fill the buffer with appropriate vertices to describe the rectangle.
    // The rectangle will be made from two triangles...
    // Create a pointer to the first vertex in the buffer
    // Also lock it, so nothing else can touch it while the values are being
    // inserted.
    CUSTOMVERTEX *pVertices;
    if (FAILED(g_pVBSquare->Lock(0, 0, (void **)&pVertices, 0))) {
        return E_FAIL; // if the pointer to the vertex buffer could not be
                       // established.
    }

    // Fill the vertex buffers with data...
    // Triangle 1.
    pVertices[0] = { 0,  0, 0, Colors::RED };
    pVertices[1] = { 0, 10, 0, Colors::YELLOW };
    pVertices[2].x = 10;
    pVertices[2].y = 0;
    pVertices[2].z = 0;
    pVertices[2].colour = 0x00ff0000; 
    // Triangle 2.
    pVertices[3].x = 10;
    pVertices[3].y = 0;
    pVertices[3].z = 0;
    pVertices[3].colour = 0x00ff0000; // (red)
    pVertices[4].x = 0;
    pVertices[4].y = 10;
    pVertices[4].z = 0;
    pVertices[4].colour = 0x00ffff00; // (yellow)
    pVertices[5].x = 10;
    pVertices[5].y = 10;
    pVertices[5].z = 0;
    pVertices[5].colour = 0x0000ff00; // (green)

    // Unlock the vertex buffer...
    g_pVBSquare->Unlock();
    return S_OK;
}

HRESULT SetupCrossVertices() {
    int crossVertices = 6 * 3;
    int BufferSize = crossVertices * sizeof(CUSTOMVERTEX);
    if (FAILED(g_pd3dDevice->CreateVertexBuffer(
            BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pVBCross,
            NULL))) {
        return E_FAIL;
    }
    CUSTOMVERTEX *pVertices;
    if (FAILED(g_pVBCross->Lock(0, 0, (void **)&pVertices, 0))) {
        return E_FAIL;
    }

    // Triangle 1
    pVertices[0].x = 1;
    pVertices[0].y = 0;
    pVertices[0].z = 0;
    pVertices[0].colour = 0x00ff0000; // (red)
    pVertices[1].x = 5;
    pVertices[1].y = 6;
    pVertices[1].z = 0;
    pVertices[1].colour = 0x00ff0000;
    pVertices[2].x = 6;
    pVertices[2].y = 5;
    pVertices[2].z = 0;
    pVertices[2].colour = 0x00ff0000;
    // Triangle 2
    pVertices[3].x = 1;
    pVertices[3].y = 0;
    pVertices[3].z = 0;
    pVertices[3].colour = 0x0000ff00;
    pVertices[4].x = 0;
    pVertices[4].y = 1;
    pVertices[4].z = 0;
    pVertices[4].colour = 0x0000ff00;
    pVertices[5].x = 5;
    pVertices[5].y = 6;
    pVertices[5].z = 0;
    pVertices[5].colour = 0x0000ff00;
    // Triangle 3
    pVertices[6].x = -6;
    pVertices[6].y = 5;
    pVertices[6].z = 0;
    pVertices[6].colour = 0x00ffff00;
    pVertices[7].x = -5;
    pVertices[7].y = 6;
    pVertices[7].z = 0;
    pVertices[7].colour = 0x00ffff00;
    pVertices[8].x = 6;
    pVertices[8].y = -5;
    pVertices[8].z = 0;
    pVertices[8].colour = 0x00ffff00;
    // Triangle 4
    pVertices[9].x = 6;
    pVertices[9].y = -5;
    pVertices[9].z = 0;
    pVertices[9].colour = 0x0000ffff;
    pVertices[10].x = 5;
    pVertices[10].y = -6;
    pVertices[10].z = 0;
    pVertices[10].colour = 0x0000ffff;
    pVertices[11].x = -6;
    pVertices[11].y = 5;
    pVertices[11].z = 0;
    pVertices[11].colour = 0x0000ffff;
    // Triangle 5
    pVertices[12].x = -1;
    pVertices[12].y = 0;
    pVertices[12].z = 0;
    pVertices[12].colour = 0x00ff00ff;
    pVertices[13].x = 0;
    pVertices[13].y = -1;
    pVertices[13].z = 0;
    pVertices[13].colour = 0x00ff00ff;
    pVertices[14].x = -5;
    pVertices[14].y = -6;
    pVertices[14].z = 0;
    pVertices[14].colour = 0x00ff00ff;
    // Triangle 6
    pVertices[15].x = -5;
    pVertices[15].y = -6;
    pVertices[15].z = 0;
    pVertices[15].colour = 0x00ffffff;
    pVertices[16].x = -6;
    pVertices[16].y = -5;
    pVertices[16].z = 0;
    pVertices[16].colour = 0x00ffffff;
    pVertices[17].x = -1;
    pVertices[17].y = 0;
    pVertices[17].z = 0;
    pVertices[17].colour = 0x00ffffff;
    // Unlock the vertex buffer...
    g_pVBCross->Unlock();
    return S_OK;
}

HRESULT SetupGeometry() {
    if (FAILED(SetupSquareVertices()) || FAILED(SetupCrossVertices()) 
        || FAILED(Setup3DStepVertices()) || FAILED(SetupConeVertices())
        || FAILED(SetupSphereVerticesPoints())
		|| FAILED(SetupSphereVerticesTris())) {
        return E_FAIL;
    }
    return S_OK;
}

//-----------------------------------------------------------------------------
// Set up the view - the view and projection matrices.

void SetupViewMatrices() {
    // Set up the view matrix.
    // This defines which way the 'camera' will look at, and which way is up.
    float cameraX = 
        gCameraDistance * cosf(gCameraPolar) * cosf(gCameraAzimuth);
    float cameraY = gCameraDistance * sinf(gCameraPolar);
    float cameraZ =
        gCameraDistance * cosf(gCameraPolar) * -sinf(gCameraAzimuth);
    D3DXVECTOR3 vCamera(cameraX, cameraY, cameraZ);
    D3DXVECTOR3 vLookat(0.0f, 0.0f, 0.0f);
    D3DXVECTOR3 vUpVector(0.0f, 1.0f, 0.0f);
    D3DXMATRIX matView;
    D3DXMatrixLookAtLH(&matView, &vCamera, &vLookat, &vUpVector);
    g_pd3dDevice->SetTransform(D3DTS_VIEW, &matView);

    // Set up the projection matrix.
    // This transforms 2D geometry into a 3D space.
    D3DXMATRIX matProj;
    D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI / 4, 1.0f, 1.0f, 10000.0f);
    g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &matProj);
}

//-----------------------------------------------------------------------------
// Render the scene.

void Render() {
    // Clear the backbuffer to a black color
    g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                        D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

    // Begin the scene
    if (SUCCEEDED(g_pd3dDevice->BeginScene())) {
        // Define the viewpoint.
        SetupViewMatrices();

        //// Render square
        //g_pd3dDevice->SetStreamSource(0, g_pVBSquare, 0,
        //                              sizeof(CUSTOMVERTEX));
        //g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
        //g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0,
        //                            2); // using a D3DPT_TRIANGLELIST primitive

        //// Render cross
        //g_pd3dDevice->SetStreamSource(0, g_pVBCross, 0, sizeof(CUSTOMVERTEX));
        //g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
        //g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 6);

        //// Render 3D Step
        //g_pd3dDevice->SetStreamSource(0, g_pVB3DStep, 0, sizeof(CUSTOMVERTEX));
        //g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
        //g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 20);

        //// Render Cone
        //g_pd3dDevice->SetStreamSource(0, g_pVBCone, 0, sizeof(CUSTOMVERTEX));
        //g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
        //g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 20);

        // Render Sphere points
        //g_pd3dDevice->SetStreamSource(0, g_pVBSpherePoints, 0, sizeof(CUSTOMVERTEX));
        //g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
        //g_pd3dDevice->DrawPrimitive(D3DPT_POINTLIST, 0, 22*22);
        
        // Render Sphere triangles
        g_pd3dDevice->SetStreamSource(0, g_pVBSphereTris, 0, sizeof(CUSTOMVERTEX));
        g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
        g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 962);

        // End the scene.
        g_pd3dDevice->EndScene();
    }

    // Present the backbuffer to the display.
    g_pd3dDevice->Present(NULL, NULL, NULL, NULL);
}

//-----------------------------------------------------------------------------
// The window's message handling function.
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    static const float MOUSE_SENSITIVITY = 0.01f;
    static const float MOUSE_WHEEL_SENSITIVITY = 0.1f;
    int zDelta;
    switch (msg) {
    case WM_MOUSEMOVE:
        gMouseX = GET_X_LPARAM(lParam);
        gMouseY = GET_Y_LPARAM(lParam);
        if (gbMouseSpin) {
            float deltaX = (float)gMouseX - gPrevMouseX;
            float deltaY = (float)gMouseY - gPrevMouseY;
            gCameraAzimuth += (deltaX * MOUSE_SENSITIVITY);
            gCameraPolar += (deltaY * MOUSE_SENSITIVITY);
            if (gCameraPolar > D3DX_PI/2) {
                gCameraPolar = D3DX_PI/2;
            }
            if (gCameraPolar < -D3DX_PI/2) {
                gCameraPolar = -D3DX_PI/2;
            }
            if (gCameraAzimuth > 2*D3DX_PI) {
                gCameraAzimuth -= 2*D3DX_PI;
            }
            if (gCameraAzimuth < 0) {
                gCameraAzimuth += 2*D3DX_PI;
            }
            
            gPrevMouseX = gMouseX;
            gPrevMouseY = gMouseY;
        }
        return 0;
        
    case WM_MOUSEWHEEL:
        zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
        gCameraDistance -= zDelta * MOUSE_WHEEL_SENSITIVITY;
        return 0;

    case WM_LBUTTONDOWN:
        if (gbMouseSpin) {
            gbMouseSpin = false;
        }
        else {
            //gbMouseSpin = true;
            gPrevMouseX = gMouseX;
            gPrevMouseY = gMouseY;
        }
        return 0;

    case WM_DESTROY: {
        PostQuitMessage(0);
        return 0;
    }
    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}

//-----------------------------------------------------------------------------
// WinMain() - The application's entry point.
// This sort of procedure is mostly standard, and could be used in most
// DirectX applications.

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int) {
    // Register the window class
    WNDCLASSEX wc = {sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     "Basic D3D Example", NULL};
    RegisterClassEx(&wc);

    // Create the application's window
    HWND hWnd = CreateWindow("Basic D3D Example", "Solid Rectangle",
                             WS_OVERLAPPEDWINDOW, 200, 200, 700, 700,
                             GetDesktopWindow(), NULL, wc.hInstance, NULL);

    // Initialize Direct3D
    if (SUCCEEDED(SetupD3D(hWnd))) {
        // Create the scene geometry
        if (SUCCEEDED(SetupGeometry())) {
            // Show the window
            ShowWindow(hWnd, SW_SHOWDEFAULT);
            UpdateWindow(hWnd);

            // Enter the message loop
            MSG msg;
            ZeroMemory(&msg, sizeof(msg));
            while (msg.message != WM_QUIT) {
                if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE)) {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                } else
                    Render();
            }
        }

        CleanUp();
    }

    UnregisterClass("Basic D3D Example", wc.hInstance);
    return 0;
}
