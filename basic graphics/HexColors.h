namespace Colors {
const DWORD RED        = 0x00ff0000;
const DWORD GREEN      = 0x0000ff00;
const DWORD BLUE       = 0x000000ff;
const DWORD YELLOW     = 0x00ffff00;
const DWORD CYAN       = 0x0000ffff;
const DWORD MAGENTA    = 0x00ff00ff;
const DWORD BLACK      = 0x00000000;
const DWORD WHITE      = 0x00ffffff;
const DWORD TWILIGHT   = 0x00dcaeee;
const DWORD APPLEJACK  = 0x00ffc36b;
const DWORD FLUTTERSHY = 0x00fcf4a4;
}
